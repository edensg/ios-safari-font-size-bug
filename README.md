# Possible iOS safari font size bug?

Applies to iOS Safari (and presumably other iOS web views) — tested only on emulated iOS 13.3 on MacOS so far

![screenshot of issue with font size](screenshot.png)

## Explanation

No font size css rules are specified, however certain elements seem to gain an extra 6 or 7 pixels of font size from the root font size.

Here's some data I collected:

| root font size | affected element’s font size |
| -------------- | ---------------------------- |
| 20px           | 26px                         |
| 19px           | 25px                         |
| 18px           | 24px                         |
| 17px           | 23px                         |
| 16px           | 23px                         |
| 15px           | 22px                         |
| 14px           | 21px                         |
| 13px           | 20px                         |
| 12px           | 19px                         |
| 11px           | 18px                         |

## Instructions

```sh
yarn # install dependencies
yarn dev # easiest way to view page
```

## To Do

- [ ] isolate issue from `react-ticker`
- [ ] isolate issue from React (re-implement in vanilla js)

Demo deployed at [ios-safari-font-size-bug.esg.now.sh](https://ios-safari-font-size-bug.esg.now.sh/).
